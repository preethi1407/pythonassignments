# Python program to check for balanced brackets using previously created linked list as a stack

class Node:
# this method is used to create a node in a linked list
  def __init__(self,data):
    self.data = data
    self.next = None

class Linkedlist:
  def __init__(self):
    self.head = None 
  
  def insertion_at_beginning(self,data):
    # this method is used to insert at the beginning of a linked list
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
  
  def insertion_at_end(self,data):
    # this method is used to insert at the end of a linked list
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
  
  def insertion_at_specified_position(self,pos,data):
    # this method is used to insert at the position specified by the user in a linked list
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
  
  def deletion_at_beginning(self):
    # this method is used to delete at the beginning of a linked list
    temp = self.head 
    self.head = temp.next 
    temp.next = None
  
  def deletion_at_end(self):
    # this method is used to delete at the end of a linked list
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
  
  def deletion_at_specified_position(self,pos):
    # this method is used to delete the node at the position specified by the user in a linked list
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
  
  def display(self):
    # this method is used to display all the nodes present in the linked list
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      new = ""
      while(temp):
        new+=temp.data
        temp=temp.next 
      return new

def is_balanced(expr):
    # this function is used to check if the braces in an expression are balanced or not
	stack = []

	# Traverse the Expression
	for char in expr:
		if char in ["(", "{", "["]:
			stack.append(char)
		else:

			if not stack:
				return False
			current_char = stack.pop()
			if current_char == '(':
				if char != ")":
					return False
			if current_char == '{':
				if char != "}":
					return False
			if current_char == '[':
				if char != "]":
					return False
	if stack:
		return False
	return True

# creating an instance for the Linked_list class
linked_list = Linkedlist()
# creating nodes in a linked list
node = Node('(')
linked_list.head = node
node1 = Node('{')
node.next = node1 
node2 = Node('}')
node1.next = node2 
node3 = Node(')')
node2.next = node3 
# linked_list.insertion_at_beginning('[')
linked_list.insertion_at_end(')')
linked_list.deletion_at_end()
x = linked_list.display()

expr = x
print(expr)

#calling the function
if is_balanced(expr):
	print("Balanced")
else:
	print("Not Balanced")
