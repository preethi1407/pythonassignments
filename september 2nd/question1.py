class Node:
  def __init__(self,data):
    # this method is used to create a node in a linked list
    self.data = data
    self.next = None

class Linked_list:
  def __init__(self):
    self.head = None 
  
  def insertion_at_beginning(self,data):
    # this method is used to insert at the beginning of a linked list
    new_node = Node(data)
    new_node.next = self.head 
    self.head = new_node
  
  def insertion_at_end(self,data):
    # this method is used to insert at the end of a linked list
    new_node = Node(data)
    temp = self.head 
    while temp.next:
      temp = temp.next  
    temp.next = new_node
  
  def insertion_at_specified_position(self,pos,data):
    # this method is used to insert at the position specified by the user in a linked list
    new_node = Node(data)
    temp = self.head 
    for i in range(pos-1):
      temp = temp.next 
    new_node.data = data 
    new_node.next = temp.next
    temp.next = new_node
  
  def deletion_at_beginning(self):
    # this method is used to delete at the beginning of a linked list
    temp = self.head 
    self.head = temp.next 
    temp.next = None
  
  def deletion_at_end(self):
    # this method is used to delete at the end of a linked list
    prev = self.head 
    temp = self.head.next 
    while temp.next is not None:
      temp = temp.next 
      prev = prev.next 
    prev.next = None
  
  def deletion_at_specified_position(self,pos):
    # this method is used to delete the node at the position specified by the user in a linked list
    prev = self.head 
    temp = self.head.next 
    for i in range(1,pos-1):
      temp = temp.next 
      prev = prev.next 
    prev.next = temp.next
  
  def display(self):
    # this method is used to display all the nodes present in the linked list
    if self.head is None:
      print("Empty LinkedList")
    else:
      temp = self.head 
      while(temp):
        print(temp.data , end = " ")
        temp=temp.next 

# creating an instance for the Linked_list class
linked_list = Linked_list()
# creating nodes in a linked list
node = Node(1)
linked_list.head = node
node1 = Node(2)
node.next = node1 
node2 = Node(3)
node1.next = node2 
node3 = Node(4)
node2.next = node3 
linked_list.insertion_at_beginning(0)
linked_list.insertion_at_end(0)
linked_list.insertion_at_specified_position(1,9)
linked_list.deletion_at_beginning()
linked_list.deletion_at_end()
linked_list.deletion_at_specified_position(2)
linked_list.display()
