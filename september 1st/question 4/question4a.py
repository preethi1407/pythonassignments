#Python program to sort (ascending and descending) a dictionary by value

import operator

#input dictionary
ex_dict = {1: 14, 3: 32, 4: 63, 2: 54, 0: 98}

#use the sorted method to sort the dictionary by value
asc = dict(sorted(ex_dict.items(), key=operator.itemgetter(1)))
print('Ascending order : ',asc)

#for the descending order by keeping the value of reverse to True  
desc = dict( sorted(ex_dict.items(), key=operator.itemgetter(1),reverse=True))
print('Descending order : ',desc)
