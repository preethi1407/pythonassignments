# Create a dictionary with “n” elements where the keys range from 1 to n and values are squares of the keys respectively

#taking the input
n=int(input("Enter the number of key-value pairs you want to print : " ))

#take the empty dictionary to store the resultant value
d = dict()

for x in range(1,n+1):
    d[x]=x*x

#printing the resultant dictionary
print(d) 
