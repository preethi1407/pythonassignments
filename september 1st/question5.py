import os
import shutil

# creating 100 new files in a folder in .txt format
def creating_files():
    a = 0
    while a < 100:
        a += 1
        path = r"C:\Users\pkommabathula\Desktop\old_files"
        file_name = str(a)+'.txt'
        with open(os.path.join(path, file_name), 'w') as fp:
            fp.write('Hello')

# converting the 100 files created into csv format into a new folder with new file name
def copying_renaming_files():
    src = r"C:\Users\pkommabathula\Desktop\old_files"
    des = r"C:\Users\pkommabathula\Desktop\new_files"
    prefix = input("enter prefix : ")
    suffix = int(input("start numbering from : "))

    for file_name in os.listdir(src):
        src_file = os.path.join(src,file_name)
        des_file = os.path.join(des,prefix+str(suffix)+".csv")
        suffix += 1
        shutil.copy(src_file,des_file)

creating_files()
copying_renaming_files()

