# reading the file name
file_name = input("enter the file name : ")
extention = input("enter the type of extention for the file name you just entered :")

#Reversing the filename and storing the value in the new variable file_name_new
file_name_new = file_name[::-1]
f1 = open(file_name_new+extention, "w")

# Open the input file and get the content into a variable data
with open(file_name+extention, "r") as myfile:
	data = myfile.read()

# For Full Reversing we will store the value of  data into new variable resultant_data in a reverse order
resultant_data = data[::-1]

#writing the resultant_data data into the newfile
f1.write(resultant_data)

# closing the opened file
f1.close()

